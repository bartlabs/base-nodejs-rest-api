# Base NodeJS REST API

NodeJS Express.js starter kit

## Quick start

```bash
1. git clone https://bartlabs@bitbucket.org/bartlabs/base-nodejs-rest-api.git <YOUR_PROJECT_NAME>
2. cd <YOUR_PROJECT_NAME>
3. git submodule update --init --recursive
4. npm install
5. Create .env file from .env-sample
6. npm run api

# Rename current base remote and add your origin
7. git remote rename origin base
8. git remote add origin <REPO-URL>
```

in your browser go to *http://localhost:(PORT)**

*(PORT) is in the `.env` file - default is 3002

___

## Samples

`http://localhost:(PORT)/v1/sample/Yeah!` - Sample GET endpoint with query param

`http://localhost:(PORT)/v1/sample-auth/Yeah!` - Sample GET endpoint with query param and [JWT authentication](#markdown-header-JWT-authentication)

`http://localhost:(PORT)/v1/sample-external` - Sample GET endpoint from [external API](#markdown-header-responses-from-external-api)

`http://localhost:(PORT)/v1/api-docs/` - Sample REST API documentation

___

## Usage

#### TODO...


