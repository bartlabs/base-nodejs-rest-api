import merge from 'lodash/merge';

import { SWAGGER_DEFAULT_OPTIONS } from '@fw/config';

const SWAGGER_VERSION_OPTIONS: any = {
    apis: [
        './api/v1/index.ts',
        './api/v1/models/**/*.ts',
        './fw/api/models/**/*.ts', // Don't remove this path
      ],
      swaggerDefinition: {
        info: {
          contact: {
            email: 'kontakt@bartlab.pl',
          },
          title: 'Places API',
          version: '1.0.0',
        },
      },
};

export const SWAGGER_OPTIONS = merge(SWAGGER_DEFAULT_OPTIONS, SWAGGER_VERSION_OPTIONS);
