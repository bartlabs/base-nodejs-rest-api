import { FwAuthorizeUserModel } from '@fw/models/authorizedUsers.model';

export const AUTHORIZED_USERS: FwAuthorizeUserModel[] = [
    {
        email: 'email',
        pass: 'password',
        username: 'username',
    },
];
