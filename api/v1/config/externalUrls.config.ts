// Samples:
const hosts = {
    githubSampleApi: 'https://api.github.com/',
};

export const EXTERNAL_URLS_CONFIG = {
    '/sample-external': hosts.githubSampleApi + 'users/github',
};
