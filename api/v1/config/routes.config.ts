import {
    FwHttpMethodsEnum as Method, FwParamLocationsEnum as Location,
} from '@fw/enums';
import { FwRoutesParamNamesEnum as Param } from '@v1/enums';

// tslint:disable
type FwRoutesType = {}; // Fake overvite type

// tslint:disable: object-literal-sort-keys
export const ROUTES_CONFIG: FwRoutesType = {
    '/': {
        methods: {
            [Method.GET]: {}
        },
    },
    '/sample/{chars}': {
        methods: {
            [Method.GET]: {
                [Location.PATH]: {
                    [Param.chars]: {
                        required: true,
                        type: 'string',
                        onlyLetters: true,
                        minLength: 3,
                        maxLength: 50,
                    },
                },
            },
        },
    },
    '/sample-auth/{chars}': {
        methods: {
            [Method.GET]: {
                [Location.PATH]: {
                    [Param.chars]: {
                        required: true,
                        type: 'string',
                        onlyLetters: true,
                        minLength: 3,
                        maxLength: 50,
                    },
                },
            },
        },
    },    
    '/sample-external': {
        methods: {
            [Method.GET]: {}
        },
    },
};
