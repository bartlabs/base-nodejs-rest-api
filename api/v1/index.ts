import express from 'express';
import get from 'lodash/get';

import { FwResponder, FwSwagger } from '@fw/controllers';
import { FwAuthorizationer, FwExternaler, FwRequester } from '@fw/middlewares';
import { FwModuleResponseModel } from '@fw/models';
import { AUTHORIZED_USERS, EXTERNAL_URLS_CONFIG, ROUTES_CONFIG, SWAGGER_OPTIONS } from '@v1/config';

import { TestController } from './controllers';

const app = express();

const auth = new FwAuthorizationer(AUTHORIZED_USERS);
const requestsChecker = new FwRequester(ROUTES_CONFIG).checker;
const externalRequests = new FwExternaler(EXTERNAL_URLS_CONFIG).init;

/**
 * @swagger
 * tags:
 *   name: Sampler
 */

/**
 * @swagger
 * /:
 *   get:
 *     summary: Default endpoint.
 *     tags: [Sampler]
 *     responses:
 *       200:
 *         description: "Hello response :)"
 */
app.get('/', (req, res) => res.send('Hello World! :)'));

/**
 * @swagger
 * tags:
 *   name: Sampler
 */

/**
 * @swagger
 * /sample/{chars}:
 *   get:
 *     summary: Sample endpoint with query param.
 *     parameters:
 *       - in: path
 *         name: chars
 *         description: "Sample search query string"
 *     tags: [Sampler]
 *     responses:
 *       200:
 *         description: "Sample response"
 */

app.get('/sample/:chars',
    requestsChecker, (request, response) => {
        try {
            const something: FwModuleResponseModel = TestController.getSomething(request.params.chars);
            FwResponder.moduleResponder(response, something);
        } catch (err) {
            FwResponder.errorCatcher(response, err, true);
        }
});

/**
 * @swagger
 * /sample-auth/{chars}:
 *   get:
 *     summary: Sample endpoint with query param and JWT authentication
 *     parameters:
 *       - in: path
 *         name: chars
 *         description: "Sample search query string"
 *     tags: [Sampler]
 *     responses:
 *       200:
 *         description: "Sample response"
 */

app.get('/sample-auth/:chars',
    auth.verify, requestsChecker, (request, response) => {
        try {
            const something: FwModuleResponseModel = TestController.getSomething(request.params.chars);
            FwResponder.moduleResponder(response, something);
        } catch (err) {
            FwResponder.errorCatcher(response, err, true);
        }
});

/**
 * @swagger
 * /sample-external:
 *   get:
 *     summary: Sample endpoint for external api.
 *     tags: [Sampler]
 *     responses:
 *       200:
 *         description: "Data from external api"
 */
app.get('/sample-external',
    externalRequests, (request, response) => {
        try {
            const externalBody = get(response, 'external_body');
            FwResponder.moduleResponder(response, externalBody);
        } catch (err) {
            FwResponder.errorCatcher(response, err, true);
        }
});

/**
 * @swagger
 * tags:
 *   name: Auth
 */

/**
 * @swagger
 * /auth/login:
 *   post:
 *     summary: Get user authentication token.
 *     parameters:
 *       - in: body
 *         schema:
 *           $ref: '#/definitions/login'
 *     tags: [Auth]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: User authentication token.
 *         schema:
 *           $ref: '#/definitions/token'
 */
app.post('/auth/login', auth.sign); // get JWT token

if (app.settings.env === 'dev') {

    // TMP
    app.get('/userhash', (request, response) => {

        let username = AUTHORIZED_USERS[0].username;
        let pass = AUTHORIZED_USERS[0].pass;

        if (request.query.username && request.query.pass) {
            username = request.query.username;
            pass = request.query.pass;
        }

        if (username || pass) {
            const userToken =
                auth.getUserToken(username, pass) || false;
            response.send(userToken || 'Invalid parameters or no such user found.');
        } else {
            response.send('Required parameters are missing.');
        }
    });

    new FwSwagger(app, ROUTES_CONFIG, SWAGGER_OPTIONS).init();
}

export default app;
