import { FwModuleResponseModel } from '@fw/models';

class TestController {
    getSomething(chars: string): FwModuleResponseModel {
        const something = {
            body: 'It works! Your param: ' + chars,
            status: true,
        };
        return something;
    }
}

export default new TestController();
