// tslint:disable: no-console
// tslint:disable: no-var-requires
require('module-alias/register');

'use strict';
import bodyParser from 'body-parser';
import express from 'express';

import { ENV } from '@fw/env';
import { FwEnver } from '@fw/middlewares';

import v1 from './v1/index';

const app = express();
const envChecker = new FwEnver().checker;

app.use(bodyParser.json());

app.use('*', envChecker, (req, res, next) => {
    res.locals.config = [];

    // res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', ['GET', 'POST']);

    next();
});

app.use('/v1', v1);
// app.use('/v2', v2);
// app.use('/', v1); // Default version

app.set('port', ENV.port);

const appPort = app.get('port');
const appEnv = app.get('env');

app.listen(appPort, () =>
    console.log('Server "'  + appEnv + '" started and is listening on port ' + appPort),
);
